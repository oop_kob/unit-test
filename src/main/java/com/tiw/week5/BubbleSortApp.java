package com.tiw.week5;

public class BubbleSortApp {
    public static void main(String[] args) {
        int arr[] = new int[1000];
        SelectionSortApp.randomArr(arr);
        SelectionSortApp.print(arr);
        bubbleSort(arr);
        SelectionSortApp.print(arr);
    }

    public static void bubble(int[] arr, int first, int second) {
        for (int i = first; i < second; i++) {
            if (arr[i] > arr [i+1]) {
                SelectionSortApp.swap(arr, i, i+1);
            }
        }
    }

    public static void bubbleSort(int[] arr) {
        for (int i = arr.length-1; i > 0; i--) {
            bubble(arr, 0, i);
        }
    }



}
